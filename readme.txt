bitbucket repository
	https://bitbucket.org/vasnabilisim/com.homework.openweather

download project
	git clone git@bitbucket.org:vasnabilisim/com.homework.openweather.git
	git clone https://vasnabilisim@bitbucket.org/vasnabilisim/com.homework.openweather.git

goto directory
	com.homework.openweather

build source
	mvn compile
	mvn test

run server
	mvn spring-boot:run

test services
	http://localhost:8080/weather?cityId=2643743
		Returns weather info of the city
	http://localhost:8080/city?name=london&size=20
		Searches cities

test page
	http://localhost:8080/page/input
		Select city. Weather info of the city will be shown.


TODO
	Cache
	Call external services handling timeout
	Configuration for all hardcoded urls and ids