package com.homework.openweather.external.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestTemplate;

import com.homework.openweather.external.dto.OpenWeatherResult;

/**
 * @author MFG
 */
@Service
public class OpenWeatherClient {

    Logger log;

    @Autowired
    RestTemplate restTemplate;

    public OpenWeatherClient() {
        log = LoggerFactory.getLogger(OpenWeatherClient.class);
    }
    
    OpenWeatherClient(Logger log, RestTemplate restTemplate) {
        this.log = log;
        this.restTemplate = restTemplate;
    }
    
    public OpenWeatherResult getWeather(int cityId) {
        //TODO extract base url and appid as config param.
        //TODO consider using concurency for timeouts and such
        String url = "https://api.openweathermap.org/data/2.5/weather?APPID=bce0a057b469d74650912a250124d3f6&id=" + cityId;
        try {
            return restTemplate.getForObject(url, OpenWeatherResult.class);
        } catch (Exception e) {
            String message = "Open weather call failed for cityId: " + cityId;
            log.error("message", e);
            throw new OpenWeatherException(message, e);
        }
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    static class OpenWeatherException extends RuntimeException {
        private static final long serialVersionUID = 2905893289157905792L;

        private OpenWeatherException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
