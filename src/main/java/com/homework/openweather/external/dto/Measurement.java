package com.homework.openweather.external.dto;

import java.io.Serializable;

/**
 * Basic weather measurements.
 * @author MFG
 */
public class Measurement implements Serializable {
    private static final long serialVersionUID = 2010097299633044459L;

    private float temp;
    private int pressure;
    private int humidity;
    private float temp_min;
    private float temp_max;

    public Measurement() {
    }

    public Measurement(float temp, int pressure, int humidity, float temp_min, float temp_max) {
        super();
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
    }

    public float getTemp() {
        return temp;
    }

    public int getPressure() {
        return pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public float getTemp_max() {
        return temp_max;
    }
}
