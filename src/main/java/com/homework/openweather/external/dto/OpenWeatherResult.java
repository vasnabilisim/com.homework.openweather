package com.homework.openweather.external.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents a successful response from Open Weather.
 * @author MFG
 */
@JsonIgnoreProperties({"coord", "base", "visibility", "wind", "clouds"})
public class OpenWeatherResult implements Serializable {
    private static final long serialVersionUID = 8170371903618256978L;

    private int id;
    private String name;
    private int cod;
    private Weather[] weather;
    private Measurement main;
    private long dt;
    private Sys sys;

    public OpenWeatherResult() {
    }

    public OpenWeatherResult(int id, String name, int cod, Weather[] weather, Measurement main, long dt, Sys sys) {
        this.id = id;
        this.name = name;
        this.cod = cod;
        this.weather = weather;
        this.main = main;
        this.dt = dt;
        this.sys = sys;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCod() {
        return cod;
    }

    public Weather[] getWeather() {
        return weather;
    }

    public Measurement getMain() {
        return main;
    }

    public long getDt() {
        return dt;
    }

    public Sys getSys() {
        return sys;
    }
}
