package com.homework.openweather.external.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Used to get sunset and sunrise
 * @author MFG
 */
@JsonIgnoreProperties({"type", "id", "message", "country"})
public class Sys implements Serializable {
    private static final long serialVersionUID = 1744740492099525751L;
    
    private long sunrise;
    private long sunset;

    public Sys() {
    }

    public Sys(long sunrise, long sunset) {
        super();
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public long getSunrise() {
        return sunrise;
    }

    public long getSunset() {
        return sunset;
    }
}
