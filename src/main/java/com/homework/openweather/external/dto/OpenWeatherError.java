package com.homework.openweather.external.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents an error from Open Weather.
 * @author MFG
 */
@JsonIgnoreProperties({"coord", "base", "visibility", "wind", "clouds"})
public class OpenWeatherError implements Serializable {
    private static final long serialVersionUID = 3957130648879795881L;

    private int cod;
    private String message;

    public OpenWeatherError() {
    }

    public OpenWeatherError(int cod, String message) {
        this.cod = cod;
        this.message = message;
    }

    public int getCod() {
        return cod;
    }

    public String getMessage() {
        return message;
    }
}
