package com.homework.openweather.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Represents weather information of a city for a specific day.
 * @author MFG
 */
public class WeatherInfo implements Serializable {
    private static final long serialVersionUID = 1035605517188099668L;

    private final LocalDate date;
    private final City city;
    private final String description;
    private final BigDecimal tempratureCelcius;
    private final BigDecimal tempratureFahrenheit;
    @JsonFormat(pattern = "KK:mm a")
    private final LocalTime sunrise;
    @JsonFormat(pattern = "KK:mm a")
    private final LocalTime sunset;
    
    public WeatherInfo(LocalDate date, City city, String description, BigDecimal tempratureCelcius,
            BigDecimal tempratureFahrenheit, LocalTime sunrise, LocalTime sunset) {
        this.date = date;
        this.city = city;
        this.description = description;
        this.tempratureCelcius = tempratureCelcius;
        this.tempratureFahrenheit = tempratureFahrenheit;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    public LocalDate getDate() {
        return date;
    }

    public City getCity() {
        return city;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getTempratureCelcius() {
        return tempratureCelcius;
    }

    public BigDecimal getTempratureFahrenheit() {
        return tempratureFahrenheit;
    }

    public LocalTime getSunrise() {
        return sunrise;
    }

    public LocalTime getSunset() {
        return sunset;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
          return false;
        }
        WeatherInfo rhs = (WeatherInfo) obj;
        return new EqualsBuilder().append(date, rhs.date).append(city, rhs.city).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(date).append(city).hashCode();
    }
}
