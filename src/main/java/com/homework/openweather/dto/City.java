package com.homework.openweather.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents a city on the globe.
 * @author MFG
 */
@JsonIgnoreProperties({"coord"})
public class City implements Serializable, Comparable<City> {
    private static final long serialVersionUID = -7042999830394649376L;

    private int id;
    private String name;
    private String country;
    
    public City() {
    }

    public City(int id, String name, String countryCode) {
        this.id = id;
        this.name = name;
        this.country = countryCode;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
          return false;
        }
        City other = (City) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return name + " - " + country;
    }

    @Override
    public int compareTo(City other) {
        return this.name.compareToIgnoreCase(other.name);
    }
}
