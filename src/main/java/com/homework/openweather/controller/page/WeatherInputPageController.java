package com.homework.openweather.controller.page;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WeatherInputPageController {

    @GetMapping("/page/input")
    public String input(Model model) {
        //model.addAttribute("SunSet", dateTimeUtil.format(weather.getSunset()));
        
        return "input";
    }
}
