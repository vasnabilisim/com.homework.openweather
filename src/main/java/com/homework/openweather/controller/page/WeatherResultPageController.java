package com.homework.openweather.controller.page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.homework.openweather.dto.WeatherInfo;
import com.homework.openweather.service.WeatherService;
import com.homework.openweather.util.DateTimeUtil;

@Controller
public class WeatherResultPageController {

    @Autowired
    WeatherService weatherService;
    
    @Autowired
    DateTimeUtil dateTimeUtil;

    @GetMapping("/page/result")
    public String result(@RequestParam(name="cityId", required=true) int cityId, Model model) {
        WeatherInfo weather = weatherService.weather(cityId);
        model.addAttribute("Date", dateTimeUtil.format(weather.getDate()));
        model.addAttribute("City", weather.getCity().getName());
        model.addAttribute("Description", weather.getDescription());
        model.addAttribute("TempratureCelcius", weather.getTempratureCelcius());
        model.addAttribute("TempratureFahrenheit", weather.getTempratureFahrenheit());
        model.addAttribute("SunRise", dateTimeUtil.format(weather.getSunrise()));
        model.addAttribute("SunSet", dateTimeUtil.format(weather.getSunset()));
        
        return "result";
    }
}
