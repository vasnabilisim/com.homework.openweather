package com.homework.openweather.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.homework.openweather.dto.City;
import com.homework.openweather.util.CityUtil;

@RestController
public class CityController {

    @Autowired
    CityUtil cityUtil;

    @RequestMapping(method=RequestMethod.GET, value="/city")
    public List<City> searchCity(@RequestParam(value="name") String name, @RequestParam(value="size", defaultValue="20") int size) {
        return cityUtil.searchByName(name, size);
    }    
}
