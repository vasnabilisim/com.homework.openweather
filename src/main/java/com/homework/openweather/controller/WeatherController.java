package com.homework.openweather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.homework.openweather.dto.WeatherInfo;
import com.homework.openweather.service.WeatherService;

@RestController
public class WeatherController {

    @Autowired
    WeatherService weatherService;
    
    @RequestMapping(method=RequestMethod.GET, value="/weather")
    public WeatherInfo weather(@RequestParam(value="cityId") int cityId) {
        return weatherService.weather(cityId);
    }    
}
