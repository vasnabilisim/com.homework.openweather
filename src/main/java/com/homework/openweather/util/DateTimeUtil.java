package com.homework.openweather.util;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

/**
 * Util class for date & time conversions
 * @author MFG
 */
@Service
public class DateTimeUtil {

    private static final long SECONDS_IN_DAY = 1 * 24 * 60 * 60;
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("KK:mm a");

    public LocalDate fromSecondsToLocalDate(long epochSecond) {
        return LocalDate.ofEpochDay(epochSecond / SECONDS_IN_DAY);
    }

    public LocalTime fromSecondsToLocalTime(long epochSecond) {
        return LocalTime.ofSecondOfDay(epochSecond % SECONDS_IN_DAY);
    }

    public String format(LocalDate value) {
        return value.format(DATE_FORMATTER);
    }

    public String format(LocalTime value) {
        return value.format(TIME_FORMATTER);
    }
}
