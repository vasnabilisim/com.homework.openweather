package com.homework.openweather.util;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.homework.openweather.dto.City;

@Service
public class CityUtil {

    Logger log;

    Map<Integer, City> cities;

    public CityUtil() {
        log = LoggerFactory.getLogger(CityUtil.class);
    }
    
    CityUtil(Logger log) {
        this.log = log;
    }
    
    public void loadCities(URL cityListFile) {
        if (cityListFile == null) {
            String message = "Null city list file";
            throw new ConfigurationException(message, null);
        }
        ObjectMapper mapper = new ObjectMapper();
        City[] cities = null;
        try {
            cities = mapper.readValue(cityListFile, City[].class);
        } catch (IOException e) {
            String message = "Unable to read city list file";
            throw new ConfigurationException(message, e);
        }
        this.cities = Arrays.stream(cities).collect(Collectors.toMap(it -> it.getId(), it -> it));
    }

    public City getCity(int id) {
        if (cities == null) {
            String message = "Empty city list.";
            log.error(message);
            throw new ConfigurationException(message, null);
        }
        City city = cities.get(id);
        if (city == null) {
            String message = "Unable to find city: " + id;
            log.error(message);
            throw new RequestException(message);
        }
        return city;
    }

    public List<City> searchByName(String name, int size) {
        if (name == null) {
            String message = "Null name";
            log.error(message);
            throw new RequestException(message);
        }
        if (name.length() < 2) {
            String message = "Name is shorter than 2 chars";
            log.error(message);
            throw new RequestException(message);
        }
        if (cities == null) {
            String message = "Empty city list.";
            log.error(message);
            throw new ConfigurationException(message, null);
        }
        size = Math.max(size, 1);
        size = Math.min(size, 100);
        final String nameLowerCase = name.toLowerCase();
        return cities.values().stream()
                .filter(c -> c.getName().toLowerCase().contains(nameLowerCase))
                .limit(size)
                .sorted()
                .collect(Collectors.toList());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public static class ConfigurationException extends RuntimeException {
        private static final long serialVersionUID = 6786636129041348863L;

        private ConfigurationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public static class RequestException extends RuntimeException {
        private static final long serialVersionUID = 2910813896135499677L;

        private RequestException(String message) {
            super(message);
        }
    }
}
