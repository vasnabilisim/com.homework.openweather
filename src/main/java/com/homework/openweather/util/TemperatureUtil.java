package com.homework.openweather.util;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

/**
 * Util class for temperature conversions
 * @author MFG
 */
@Service
public class TemperatureUtil {


    public BigDecimal fromKelvinToCelcius(float k) {
        float c = k - 273.15f;
        return new BigDecimal(c).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal fromKelvinToFahrenheit(float k) {
        float f = 1.8f * (k - 273.0f) + 32.0f;
        return new BigDecimal(f).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
