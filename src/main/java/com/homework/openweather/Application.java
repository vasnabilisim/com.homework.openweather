package com.homework.openweather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.web.client.RestTemplate;

import com.homework.openweather.util.CityUtil;

@SpringBootApplication
public class Application {

    @Autowired
    CityUtil cityUtil;
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void loadCities() {
        try {
            cityUtil.loadCities(Application.class.getResource("city.list.json"));
        } catch (Exception e) {
            //TODO exit elegantly
            System.exit(-1);
        }
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
