package com.homework.openweather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homework.openweather.dto.WeatherInfo;
import com.homework.openweather.external.client.OpenWeatherClient;
import com.homework.openweather.external.dto.OpenWeatherResult;
import com.homework.openweather.util.CityUtil;
import com.homework.openweather.util.DateTimeUtil;
import com.homework.openweather.util.TemperatureUtil;

@Service
public class WeatherService {

    @Autowired
    OpenWeatherClient openWeatherClient;

    @Autowired
    CityUtil cityUtil;

    @Autowired
    TemperatureUtil temperatureUtil;

    @Autowired
    DateTimeUtil dateTimeUtil;

    WeatherService(OpenWeatherClient openWeatherClient, CityUtil cityUtil, TemperatureUtil temperatureUtil, DateTimeUtil dateTimeUtil) {
        this.openWeatherClient = openWeatherClient;
        this.cityUtil = cityUtil;
        this.temperatureUtil = temperatureUtil;
        this.dateTimeUtil = dateTimeUtil;
    }

    public WeatherInfo weather(int cityId) {
        OpenWeatherResult result = openWeatherClient.getWeather(cityId);
        WeatherInfo weatherInfo = toWeatherInfo(result);
        return weatherInfo;
    }    

    private WeatherInfo toWeatherInfo(OpenWeatherResult result) {
        return new WeatherInfo(dateTimeUtil.fromSecondsToLocalDate(result.getDt()),
                cityUtil.getCity(result.getId()),
                result.getWeather()[0].getDescription(),
                temperatureUtil.fromKelvinToCelcius(result.getMain().getTemp()),
                temperatureUtil.fromKelvinToFahrenheit(result.getMain().getTemp()),
                dateTimeUtil.fromSecondsToLocalTime(result.getSys().getSunrise()),
                dateTimeUtil.fromSecondsToLocalTime(result.getSys().getSunset()));
    }
}
