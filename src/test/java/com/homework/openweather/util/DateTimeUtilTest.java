package com.homework.openweather.util;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import com.homework.openweather.util.DateTimeUtil;

import junit.framework.Assert;

public class DateTimeUtilTest {

    final DateTimeUtil util = new DateTimeUtil();
    
    @Test
    public void fromSecondsToLocalDate() {
        final long seconds = 1529014800;
        
        LocalDate localDate = util.fromSecondsToLocalDate(seconds);
        
        Assert.assertEquals(2018, localDate.getYear());
        Assert.assertEquals(6, localDate.getMonth().getValue());
        Assert.assertEquals(14, localDate.getDayOfMonth());
    }
    
    @Test
    public void fromSecondsToLocalTime() {
        final long seconds = 1528947765;
        
        LocalTime localTime = util.fromSecondsToLocalTime(seconds);
        
        Assert.assertEquals(3, localTime.getHour());
        Assert.assertEquals(42, localTime.getMinute());
        Assert.assertEquals(45, localTime.getSecond());
    }
}
