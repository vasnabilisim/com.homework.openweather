package com.homework.openweather.util;

import java.math.BigDecimal;

import org.junit.Test;

import com.homework.openweather.util.TemperatureUtil;

import junit.framework.Assert;

public class TemperatureUtilTest {

    final TemperatureUtil util = new TemperatureUtil();
    
    @Test
    public void fromKelvinToCelcius() {
        float k = 288.23f;
        
        BigDecimal c = util.fromKelvinToCelcius(k);
        
        Assert.assertEquals(new BigDecimal("15.08"), c);
    }
    
    @Test
    public void fromKelvinToFahrenheit() {
        float k = 288.23f;
        
        BigDecimal f = util.fromKelvinToFahrenheit(k);
        
        Assert.assertEquals(new BigDecimal("59.41"), f);
    }
}
