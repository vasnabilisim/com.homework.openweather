package com.homework.openweather.util;

import java.net.URL;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

import com.homework.openweather.dto.City;
import com.homework.openweather.util.CityUtil;
import com.homework.openweather.util.CityUtil.ConfigurationException;
import com.homework.openweather.util.CityUtil.RequestException;

import junit.framework.Assert;

public class CityUtilTest {

    @Test
    public void loadCities_succeeds_whenFileIsValid() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));
        URL cityListFile = CityUtilTest.class.getResource("city.list.valid.json");
        
        util.loadCities(cityListFile);

        Assert.assertNotNull(util.cities);
        Assert.assertEquals(7, util.cities.size());
        Assert.assertEquals("Hurzuf", util.cities.get(707860).getName());
    }
    
    @Test(expected=ConfigurationException.class)
    public void loadCities_throwsException_whenFileIsNull() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));
        
        util.loadCities(null);
    }
    
    @Test(expected=ConfigurationException.class)
    public void loadCities_throwsException_whenFileIsInvalid() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));
        URL cityListFile = CityUtilTest.class.getResource("city.list.invalid.json");
        
        util.loadCities(cityListFile);
    }

    @Test
    public void getCity_succeeds_whenCityIdIsValid() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));
        URL cityListFile = CityUtilTest.class.getResource("city.list.valid.json");
        
        util.loadCities(cityListFile);
        City city = util.getCity(707860);

        Assert.assertNotNull(city);
        Assert.assertEquals("Hurzuf", city.getName());
    }

    @Test(expected=ConfigurationException.class)
    public void getCity_throwsException_whenCitiesIsEmpty() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));

        util.getCity(707860);
    }

    @Test(expected=RequestException.class)
    public void getCity_throwsException_whenCityNotFound() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));
        URL cityListFile = CityUtilTest.class.getResource("city.list.valid.json");
        
        util.loadCities(cityListFile);
        util.getCity(1);
    }

    @Test
    public void searhByName_succeeds() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));
        URL cityListFile = CityUtilTest.class.getResource("city.list.valid.json");
        
        util.loadCities(cityListFile);
        List<City> cities = util.searchByName("in", 20);
        
        Assert.assertNotNull(cities);
        Assert.assertEquals(2, cities.size());
        Assert.assertEquals("Novinki", cities.get(1).getName());
    }

    @Test
    public void searhByName_limitsNumberOfResults() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));
        URL cityListFile = CityUtilTest.class.getResource("city.list.valid.json");
        
        util.loadCities(cityListFile);
        List<City> cities = util.searchByName("in", 1);
        
        Assert.assertNotNull(cities);
        Assert.assertEquals(1, cities.size());
    }

    @Test(expected=RequestException.class)
    public void searhByName_throwsException_whenNameIsNull() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));

        util.searchByName(null, 20);
    }

    @Test(expected=RequestException.class)
    public void searhByName_throwsException_whenNameIsShorterThan2Chars() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));

        util.searchByName("u", 20);
    }

    @Test(expected=ConfigurationException.class)
    public void searhByName_throwsException_whenCitiesIsEmpty() {
        CityUtil util = new CityUtil(Mockito.mock(Logger.class));

        util.searchByName("ub", 20);
    }
}
