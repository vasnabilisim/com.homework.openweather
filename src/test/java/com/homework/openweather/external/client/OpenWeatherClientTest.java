package com.homework.openweather.external.client;

import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.homework.openweather.external.client.OpenWeatherClient;
import com.homework.openweather.external.client.OpenWeatherClient.OpenWeatherException;
import com.homework.openweather.external.dto.OpenWeatherResult;

import junit.framework.Assert;

public class OpenWeatherClientTest {

    @Test
    public void getWeather_succeeds() {
        Logger logger = Mockito.mock(Logger.class);
        OpenWeatherResult mockResult = Mockito.mock(OpenWeatherResult.class);
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(OpenWeatherResult.class))).thenReturn(mockResult);
        OpenWeatherClient client = new OpenWeatherClient(logger, restTemplate);

        OpenWeatherResult result = client.getWeather(707860);

        Assert.assertNotNull(result);
        Assert.assertEquals(mockResult, result);
    }

    @Test(expected=OpenWeatherException.class)
    public void getWeather_throwsException_whenOpenWeatherCallFails() {
        Logger logger = Mockito.mock(Logger.class);
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        RuntimeException exception = new RuntimeException();
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(OpenWeatherResult.class))).thenThrow(exception);
        OpenWeatherClient client = new OpenWeatherClient(logger, restTemplate);

        client.getWeather(707860);
    }

}
