package com.homework.openweather.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.homework.openweather.dto.City;
import com.homework.openweather.dto.WeatherInfo;
import com.homework.openweather.external.client.OpenWeatherClient;
import com.homework.openweather.external.dto.Measurement;
import com.homework.openweather.external.dto.OpenWeatherResult;
import com.homework.openweather.external.dto.Sys;
import com.homework.openweather.external.dto.Weather;
import com.homework.openweather.service.WeatherService;
import com.homework.openweather.util.CityUtil;
import com.homework.openweather.util.DateTimeUtil;
import com.homework.openweather.util.TemperatureUtil;

public class WeatherServiceTest {

    @Test
    public void weather() {
        CityUtil cityUtil = Mockito.mock(CityUtil.class);
        City city = new City(2643743, "London", "GB");
        Mockito.when(cityUtil.getCity(Mockito.anyInt())).thenReturn(city);
        OpenWeatherClient openWeatherClient = Mockito.mock(OpenWeatherClient.class);
        OpenWeatherResult openWeatherResult = new OpenWeatherResult(
                2643743, 
                "London", 
                200, 
                new Weather[] { new Weather(802, "Clouds", "scattered clouds", "03n") }, 
                new Measurement(288.23f, 1015, 52, 285.15f, 292.15f),
                1529014800, 
                new Sys(1528947765, 1529007567));
        Mockito.when(openWeatherClient.getWeather(Mockito.anyInt())).thenReturn(openWeatherResult);
        TemperatureUtil temperatureUtil = new TemperatureUtil();
        DateTimeUtil dateTimeUtil = new DateTimeUtil();
        WeatherService service = new WeatherService(openWeatherClient, cityUtil, temperatureUtil, dateTimeUtil);
        
        WeatherInfo weather = service.weather(2643743);
        
        Assert.assertNotNull(weather);
        Assert.assertEquals(LocalDate.of(2018, 6, 14), weather.getDate());
        Assert.assertEquals(city, weather.getCity());
        Assert.assertEquals("scattered clouds", weather.getDescription());
        Assert.assertEquals(new BigDecimal("15.08"), weather.getTempratureCelcius());
        Assert.assertEquals(new BigDecimal("59.41"), weather.getTempratureFahrenheit());
        Assert.assertEquals(LocalTime.of(3, 42, 45), weather.getSunrise());
        Assert.assertEquals(LocalTime.of(20, 19, 27), weather.getSunset());
    }
}
